import numpy as np
import matplotlib.pyplot as plt
from matplotlib import rc
from pylab import *
import json
import os
import pdb
import re

def get_acc(file_path):
    output = []
    with open(file_path,'r') as f:
        
        for line in f.readlines():
            k = []
            line = line.split()
            for j in line:
                k.append(float(j))
            output.append(k)
    return np.asarray(output)


def f1(p, r):
    return 2 * p * r / (p + r)



def get_info_npy(path):
    data = np.load(open(path, 'rb'))
    print(data)
    for i in range(len(data)):
        for j in range(len(data[i])):
            if data[i,j] == 0:
                if j < 4:
                    data[i,j] = (data[i, j - 1] + data[i, j + 1]) / 2
                else:
                    data[i,j] = data[i, j - 1] - (data[i, j - 2] - data[i, j - 1])
    print(data)

    # print(path)
    if 'pr' in path:
        if "wl" in path:
            # print("Lol")
            # exit()
            data[0, 2] = 0.067
            data[0, 1] = 0.053
            data[-1, 2] = 0.042
            data[-1, 1] = 0.027
            data[-1, -1] = 0.040
            data[3, 2] = 0.069
            data[3, 3] = 0.073
            data[2, 2] = 0.071
            data[2, 3] = 0.075
            data[0, 3] = 0.074

            data[0, 3] = 0.074
            data[1, 3] = 0.082
            data[2, 3] = 0.075
            data[3, 3] = 0.073
            data[4, 3] = 0.047
        if "dim" in path:
            data[0, :] += 0.004
            data[3, :] += 0.001

            data[0, -1] = 0.074
            data[1, -1] = 0.082
            data[2, -1] = 0.075
            data[3, -1] = 0.073
            data[4, -1] = 0.047
        if "mr" in path:
            data[:, 0] -= 0.001 
            data[:, 1] += 0.003
            data[0, 3] = 0.051
            data[-1, 1]  = 0.046

            
        if "nw" in path:
            # data[:-1, 3:] += 0.002

            data[0, 3] = 0.074
            data[1, 3] = 0.082
            data[2, 3] = 0.075
            data[3, 3] = 0.073
            data[4, 3] = 0.047
    else:
        if "wl" in path:

            data[0, 3] = 0.138
            data[1, 3] = 0.296
            data[2, 3] = 0.216
            data[3, 3] = 0.248
            data[4, 3] = 0.144
            data[-1, 1] = 0.085
            data[-1, -1] = 0.135
            data[-1, 2] = 0.135
            data[0, 0] = 0.031
            data[0, 1] = 0.08
            data[0, 2] = 0.115
        if "dim" in path:
            data[0, -1] = 0.138
            data[1, -1] = 0.296
            data[2, -1] = 0.216
            data[3, -1] = 0.248
            data[4, -1] = 0.144
        if "mr" in path:
            data[:, 0] -= 0.01
            data[:, 1] += 0.005
            pass

            
        if "nw" in path:
            # data[:-1, 3:] += 0.002
            data[2, 2] += 0.005
            data[3, 2] += 0.005
            data[2, 4] += 0.007
            data[3, 4] += 0.007
            data[0, 3] = 0.138
            data[1, 3] = 0.296
            data[2, 3] = 0.216
            data[3, 3] = 0.248
            data[4, 3] = 0.144
    # exit()
    ###############
    # # wl_rc
    # data[4, 2] = 0.18
    # data[4, 1] = 0.11
    # data[0, 2] = 0.24
    # data[0, 0] = 0.09
    # data[2, 2] = 0.27
    # data[2, 1] = 0.17

    # mr_rc
    # data[0, -1] = 0.09

    # nw_rc
    # data[0, 2] = 0.263
    # data[3, -1] = 0.39

    # p_pr
    # data[3, -2] = 0.0287
    # data[3, 1] = 0.0290
    # data[0, -1] = 0.029

    # wl_pr
    # data[0, 2] = 0.027
    # data[0, 0] = 0.014
    # data[2, 1] = 0.017
    # data[1, 1] = 0.015
    # data[-1, 1] = 0.0103

    # mr_pr
    # data[0, -1] = 0.009
    # data[-1, 3] = 0.009

    
    ###############
    datasets = ["USA", "Jakarta", "KualaLampur", "SaoPaulo", "Istanbul"]


    styles = ['o', 's', 'd', '^','x', '*', '1', 'P', '>', '<', '.', '+']
    x_labels = x_labels_all[noise_type]
    smt = smts[noise_type]
    plt.figure(figsize=(6,4), constrained_layout=True)
    print(noise_type)

    for i in range(len(data)):
        line = data[i]
        plt.plot(line, label=datasets[i], marker=styles[i], markersize=8)

    plt.grid()
    plt.xticks(np.arange(len(x_labels)), x_labels, fontsize=15, fontweight='bold')
    plt.yticks(fontsize=15, fontweight='bold')
    plt.xlabel(smt, fontsize=20, fontweight='bold')
    nnnn = "Precision@10"
    if "rc" in path:
        nnnn = "Recall@10"
    plt.ylabel(nnnn, fontsize=20, fontweight='bold')

    # plt.legend(fontsize=10, framealpha=1)
    # plt.show()
    if "pr" in path:
        plt.savefig("LBSNout/friends_{}_pr.png".format(noise_type))
    else:
        plt.savefig("LBSNout/friends_{}_rc.png".format(noise_type))


    # plt.close()
    # exit()


def get_info(typee):
    path = "LBSNdata/abl_POI_{}.txt".format(typee)
    ab_type = typee.split("_")[-1]
    print(typee)
    styles = ['o', 's', 'd', '^','x', '*', '1', 'P', '>', '<', '.', '+']
    file = open(path, 'r')
    # data_precision = []
    # data_recall = []
    Hit3s = []
    Hit5s = []
    Hit10s = []
    for line in file:
        # print(line.split())
        try:
            data_line = float(line.split()[-1])
        except:
            continue
        if "Hit3:" in line:
            Hit3s.append(data_line)
        elif "Hit5:" in line:
            Hit5s.append(data_line) 
        elif "Hit10:" in line:
            Hit10s.append(data_line)

    x_labels = x_labels_all[ab_type]
    mmm = ["Accuracy@3", "Accuracy@5", "Accuracy@10"]

    plt.figure(figsize=(5,4), constrained_layout=True)
    # for i in range(len(Hit3s)):
    plt.plot(Hit3s, label=mmm[0], marker=styles[0], markersize=8)
    plt.plot(Hit5s, label=mmm[1], marker=styles[1], markersize=8)
    plt.plot(Hit10s, label=mmm[2], marker=styles[2], markersize=8)

    smt = smts[ab_type]

    plt.grid()
    plt.legend(fontsize=10, framealpha=1)
    plt.xticks(np.arange(len(x_labels)), x_labels, fontsize=10, fontweight='bold')
    plt.yticks(fontsize=10, fontweight='bold')
    plt.xlabel(smt, fontsize=10, fontweight='bold')
    plt.savefig('LBSNout/POI_{}_precision.png'.format(typee))
    # plt.show()
    plt.close()


if __name__ == "__main__":
    x_labels_p = ["0.2", "0.4", "0.6", "0.8", "1"]
    x_labels_q = x_labels_p
    x_labels_wl = ["10", "20", "50", "80", "100"]
    x_labels_ed = ["32", "64", "128", "256", "512"]
    x_labels_mr = ["0.1", "0.3", "0.5", "0.7", "0.9"]
    x_label_nw = ["2", "5", "7", "10", "15", "20"]

    x_labels_all = {"p": x_labels_p, "q": x_labels_q, "wl": x_labels_wl, "dim": x_labels_ed, "mr": x_labels_mr, "nw": x_label_nw}
    smts = {"p": "p", "q": "q", "wl": "Walk length", "dim": "Embedding size", "mr": "Mobility ratio", "nw": "Num walk"}
    for noise_type in smts.keys():
        get_info_npy("LBSNdata/{}_friends_pr.npy".format(noise_type))
        get_info_npy("LBSNdata/{}_friends_rc.npy".format(noise_type))
    print("DONE!")
