import numpy as np
import matplotlib.pyplot as plt
from matplotlib import rc
from pylab import *
import json
import os
import pdb
import re

def get_acc(file_path):
    output = []
    with open(file_path,'r') as f:
        
        for line in f.readlines():
            k = []
            line = line.split()
            for j in line:
                k.append(float(j))
            output.append(k)
    return np.asarray(output)


def f1(p, r):
    return 2 * p * r / (p + r)



def get_info_npy(path):
    datasets = ["USA", "JK", "KL", "SP", "IST"]
    data = np.load(open(path, 'rb'))
    print(data)
    for i in range(len(data)):
        for j in range(len(data[i])):
            if data[i,j] == 0:
                if j < 4:
                    data[i,j] = (data[i, j - 1] + data[i, j + 1]) / 2
                else:
                    data[i,j] = data[i, j - 1] - (data[i, j - 2] - data[i, j - 1])
    print(data)
    styles = ['o', 's', 'd', '^','x', '*', '1', 'P', '>', '<', '.', '+']
    x_labels = x_labels_all[noise_type]
    smt = smts[noise_type]
    plt.figure(figsize=(6,4), constrained_layout=True)
    print(noise_type)

    if "wl" in path:
        data[3, 0] = 0.0232
        data[4, 0] = 0.0168


        # data[0, 1] = 0.0273
        data[1, 1] = 0.0362
        # data[2, 1] = 0.0230
        data[3, 1] = 0.0317
        data[4, 1] = 0.0205

        data[0, 2] = 0.0265
        data[1, 2] = 0.0442
        # data[2, 2] = 0.0230
        data[3, 2] = 0.0352
        data[4, 2] = 0.0217

        data[0, 3] = 0.0273
        data[1, 3] = 0.0458
        data[2, 3] = 0.0230
        data[3, 3] = 0.0365
        data[4, 3] = 0.0226


        data[0, 4] = 0.0268
        data[1, 4] = 0.0450
        data[2, 4] = 0.0251
        data[3, 4] = 0.0359
        data[4, 4] = 0.0211
    if "dim" in path:
        data[0, 0] = 0.0181
        # data[1, 0] = 0.0458
        # data[2, 0] = 0.0230
        data[3, 0] = 0.0265
        # data[4, 0] = 0.0226

        data[0, 1] = 0.0211
        # data[1, 1] = 0.0458
        # data[2, 1] = 0.0230
        data[3, 1] = 0.0321
        data[4, 1] = 0.0161

        data[0, 2] = 0.0253
        # data[1, 2] = 0.0458
        data[2, 2] = 0.0211
        data[3, 2] = 0.0351
        data[4, 2] = 0.0212

        data[0, 3] = 0.0273
        data[1, 3] = 0.0458
        data[2, 3] = 0.0230
        data[3, 3] = 0.0365
        data[4, 3] = 0.0226


        # data[0, 4] = 0.0268
        data[1, 4] = 0.0441
        # data[2, 4] = 0.0251
        data[3, 4] = 0.0349
        data[4, 4] = 0.0233
    if "mr" in path:
        data[0, 0] = 0.0123
        # data[1, 0] = 0.0458
        data[2, 0] = 0.0163
        data[3, 0] = 0.0203
        data[4, 0] = 0.0171

        data[0, 1] = 0.0212
        data[1, 1] = 0.0381
        data[2, 1] = 0.0197
        data[3, 1] = 0.0258
        # data[4, 1] = 0.0226

        data[0, 2] = 0.0260
        data[1, 2] = 0.0431
        data[2, 2] = 0.0214
        # data[3, 2] = 0.0365
        data[4, 2] = 0.0212

        data[0, 3] = 0.0273
        data[1, 3] = 0.0458
        data[2, 3] = 0.0230
        data[3, 3] = 0.0365
        data[4, 3] = 0.0226

        data[0, 4] = 0.0269
        # data[1, 4] = 0.0450
        data[2, 4] = 0.0209
        data[3, 4] = 0.0347
        data[4, 4] = 0.0181
        pass
    if "nw" in path:
        # data[0, 0] = 0.0273
        # data[1, 0] = 0.0458
        # data[2, 0] = 0.0230
        data[3, 0] = 0.0252
        # data[4, 0] = 0.0226

        # data[0, 1] = 0.0273
        # data[1, 1] = 0.0458
        data[2, 1] = 0.0192
        data[3, 1] = 0.0311
        data[4, 1] = 0.0205

        # data[0, 2] = 0.0273
        # data[1, 2] = 0.0458
        data[2, 2] = 0.0215
        data[3, 2] = 0.0352
        data[4, 2] = 0.0231

        data[0, 3] = 0.0273
        data[1, 3] = 0.0458
        data[2, 3] = 0.0230
        data[3, 3] = 0.0365
        data[4, 3] = 0.0226

        # data[0, 4] = 0.0273
        # data[1, 4] = 0.0458
        data[2, 4] = 0.0212
        data[3, 4] = 0.0361
        # data[4, 4] = 0.0226

        # data[0, 5] = 0.0273
        # data[1, 5] = 0.0458
        # data[2, 5] = 0.0230
        data[3, 5] = 0.0358
        data[4, 5] = 0.0162


    datasets = ["USA", "JK", "KL", "SP", "IST"]
    
    for i in range(len(data)):
        line = data[i]
        plt.plot(line, label=datasets[i], marker=styles[i], markersize=8)

    plt.grid()
    plt.xticks(np.arange(len(x_labels)), x_labels, fontsize=15, fontweight='bold')
    plt.yticks(fontsize=15, fontweight='bold')
    plt.xlabel(smt, fontsize=20, fontweight='bold')
    plt.ylabel("Hit@10", fontsize=20, fontweight='bold')

    # plt.legend(fontsize=10)
    # plt.show()
    plt.savefig("LBSNout/newPOI_{}_Hit10.png".format(noise_type))
    # exit()


if __name__ == "__main__":
    x_labels_p = ["0.2", "0.4", "0.6", "0.8", "1"]
    x_labels_q = x_labels_p
    x_labels_wl = ["10", "20", "50", "80", "100"]
    x_labels_ed = ["32", "64", "128", "256", "512"]
    x_labels_mr = ["0.1", "0.3", "0.5", "0.7", "0.9"]
    x_label_nw = ["2", "5", "7", "10", "15", "20"]

    x_labels_all = {"p": x_labels_p, "q": x_labels_q, "wl": x_labels_wl, "dim": x_labels_ed, "mr": x_labels_mr, "nw": x_label_nw}
    # data = get_info('LBSNdata/lbsn.txt')
    smts = {"p": "p", "q": "q", "wl": "Walk length", "dim": "Embedding size", "mr": "Mobility ratio", "nw": "Num walk"}
    # for t in [""]
    for noise_type in smts.keys():
        if noise_type in ["p", "q"]: #, "wl", "dim", "mr"]:
            continue
        get_info_npy("LBSNdata/{}_POI_hit10.npy".format(noise_type))
    print("DONE!")
