import numpy as np
import matplotlib.pyplot as plt
from matplotlib import rc
from pylab import *
import json
import os
import pdb
import re

def get_acc(file_path):
    output = []
    with open(file_path,'r') as f:
        
        for line in f.readlines():
            k = []
            line = line.split()
            for j in line:
                k.append(float(j))
            output.append(k)
    return np.asarray(output)


def f1(p, r):
    return 2 * p * r / (p + r)


def get_info_dcg(path = "LBSNdata/dcg.txt"):
    models = ['MSC-LBSN', 'DeepWalk S', 'DeepWalk SM', 'Node2Vec S', 'Node2Vec SM', 'Line S', "Line SM", "DHNE M", "LBSN2Vec", "HEBE", "Katz+SCos", "Katz+MinEnt"]
    datas = ["SP"]
    x_labels = ["10", "20", "50", "100", "200"]
    styles = ['o', 's', 'd', '^','x', '*', '1', 'P', '>', '<', '.', '+', '2']

    datas = []
    with open(path, 'r') as file:
        for line in file:
            dataline = line.split()
            dataline = float(dataline[0])
            datas.append(dataline)
    
    datas = np.array(datas)
    datas = datas.reshape(-1, 5)

    def draw(mode, this_precision, dataset):
        plt.figure(figsize=(6.5,5), constrained_layout=True)
        # print(len(this_precision), len(models), len(styles))
        for j in range(len(this_precision)):
            print(len(this_precision), len(models), len(styles))
            plt.plot(this_precision[j], label=models[j], marker=styles[j])
        plt.grid()
        plt.text(3.40, np.max(this_precision), s=dataset, fontweight='bold', fontsize=35)
        # plt.legend(fontsize=20, framealpha=1)
        plt.xticks(np.arange(len(x_labels)), x_labels, fontsize=20, fontweight='bold')
        plt.yticks(fontsize=20, fontweight='bold')
        plt.xlabel('K', fontsize=30, fontweight='bold')
        plt.ylabel(mode, fontsize=30, fontweight='bold')

        # h.set_rotation(0)
        plt.ylim(-0.001, np.max(this_precision) + np.max(this_precision) / 10)
        plt.savefig('LBSNout/NDCG_{}_{}.pdf'.format(dataset, mode))
        # plt.show()

    # import pdb; pdb.set_trace()
    # this_precision = datas.T
    draw("NDCG", datas, dataset="JK")



def get_info(path):
    models = ['MSC-LBSN', 'DeepWalk S', 'DeepWalk M', 'DeepWalk SM', 'Node2Vec S', 'Node2Vec M', 'Node2Vec SM', 'Line S', 'Line M', "Line SM", "DHNE M", "LBSN2Vec", "Splitter"]
    # to_remove = ["DeepWalk M", "Node2Vec M", ]
    datas = ["IST", "SP", "KL", "TKY", "USA", "NYC", "JK"]
    x_labels = ["10", "20", "50", "100", "200"]
    styles = ['o', 's', 'd', '^','x', '*', '1', 'P', '>', '<', '.', '+', '2']
    file = open(path, 'r')
    cur_index = 0
    data = dict()
    for line in file:
        if len(line.split()) < 3:
            cur_index += 1
            continue
        print(line)
        print(models[cur_index])
        if models[cur_index] not in data:
            data[models[cur_index]] = {'precision':[], 'recall':[], 'f1':[]}
        data_line = line.strip().split()
        this_precision = []
        this_recall = []
        this_f1 = []
        for ele in data_line:
            precision = float(ele.split("|")[0])
            recall = float(ele.split("|")[1])
            f1_score = f1(precision, recall)
            this_precision.append(precision)
            this_recall.append(recall)
            this_f1.append(f1_score)
        data[models[cur_index]]['precision'].append(this_precision)
        data[models[cur_index]]['recall'].append(this_recall)
        data[models[cur_index]]['f1'].append(this_f1)
    
    # new_data = {'precision':[], 'recall': [], 'f1': []}
    new_data = dict()

    for index in range(len(datas)):
        cur = datas[index]
        this_info = {'precision': [], 'recall': [], 'f1': []}
        for j in range(len(models)):
            model_j = models[j]
            model_j_precision = data[model_j]['precision'][index]
            model_j_recall = data[model_j]['recall'][index]
            model_j_f1 = data[model_j]['f1'][index]

            this_info['precision'].append(model_j_precision)
            this_info['recall'].append(model_j_recall)
            this_info['f1'].append(model_j_f1)
        new_data[cur] = this_info


    def draw(mode, this_precision):
        plt.figure(figsize=(6.5,5), constrained_layout=True)
        # print(len(this_precision), len(models), len(styles))
        for j in range(len(this_precision)):
            print(len(this_precision), len(models), len(styles))
            plt.plot(this_precision[j], label=models[j], marker=styles[j])
        plt.grid()
        plt.text(3.40, np.max(this_precision), s=dataset, fontweight='bold', fontsize=35)
        # plt.legend(fontsize=20, framealpha=1)
        plt.xticks(np.arange(len(x_labels)), x_labels, fontsize=20, fontweight='bold')
        plt.yticks(fontsize=20, fontweight='bold')
        plt.xlabel('K', fontsize=30, fontweight='bold')
        plt.ylabel(mode, fontsize=30, fontweight='bold')

        # h.set_rotation(0)
        plt.ylim(-0.001, np.max(this_precision) + np.max(this_precision) / 10)
        # plt.savefig('LBSNout/Ver2{}_{}.png'.format(dataset, mode))
        plt.show()
        exit()
        plt.close()


    for dataset in new_data:
        this_precision = new_data[dataset]['precision']
        # print(this_precision)
        for i in range(len(this_precision)):
            for j in range(4):
                if this_precision[i][j] < this_precision[i][j+1]:
                    print(dataset, models[i])
        this_recall = new_data[dataset]['recall']
        for i in range(len(this_precision)):
            for j in range(4):
                if this_recall[i][j] > this_recall[i][j+1]:
                    print(dataset, models[i])
        this_f1 = new_data[dataset]['f1']
        draw('Precision@K', this_precision)
        draw('Recall@K', this_recall)
        draw('F1score@K', this_f1)
        

    return new_data


if __name__ == "__main__":
    # data = get_info('LBSNdata/lbsn.txt')
    get_info_dcg()
    print("DONE!")
